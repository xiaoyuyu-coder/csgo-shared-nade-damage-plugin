#pragma semicolon 1

#define DEBUG

#define PLUGIN_AUTHOR "ScriptKid"
#define PLUGIN_VERSION "0.10"

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <sdkhooks>

#pragma newdecls required

EngineVersion g_Game;

public bool realFlash = true;

public Plugin myinfo = 
{
	name = "SharedNadeDamage",
	author = PLUGIN_AUTHOR,
	description = "When someones gets flashed everyone in that team gets flashed, grenade & molotov also hurt teammates.",
	version = PLUGIN_VERSION,
	url = ""
};

stock float fmax(float a, float b) {
    return a > b ? a : b;
} 

public void OnPluginStart()
{
	g_Game = GetEngineVersion();
	if(g_Game != Engine_CSGO && g_Game != Engine_CSS)
	{
		SetFailState("This plugin is for CSGO/CSS only.");	
	}
	
	HookEvent("player_blind", Event_PlayerBlind);
	HookEvent("player_hurt", Event_PlayerHurt);
}

// When player gets flashed event
public Action Event_PlayerBlind(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	// Only continue if the flash was inflicted from a real throw not from flashbangs created by this plugin
	if(realFlash) {
		
		int FlashedPlayerTeam = GetClientTeam(client);
		
		// Loop through all teammates of the player being flashed
		for (int i = 1; i <= MaxClients; i++)
		{
		    if (IsClientInGame(i) && client != i && FlashedPlayerTeam == GetClientTeam(i))
		    {
		    	// Spawn and detonate a new flashbang infront of teammates face
				SpawnFlashOnLocation(i);
		    }
		} 	
	}
	
	return Plugin_Continue;
} 

// When player gets hurt from he or molotov event
public Action Event_PlayerHurt(Handle event, const char[] name, bool dontBroadcast)
{
	char[] weapon = "";
	GetEventString(event, "weapon", weapon, 255);
	
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	int attacker = GetClientOfUserId(GetEventInt(event, "attacker"));
	int dmgH = GetEventInt(event, "dmg_health");
	int dmgA = GetEventInt(event, "dmg_armor");
	int dmg = dmgH + dmgA;
	int HurtPlayerTeam = GetClientTeam(client);

	if(StrEqual(weapon, "hegrenade") || StrEqual(weapon, "inferno")) {
		
		// Loop through all teammates and damage them also
		for (int i = 1; i <= MaxClients; i++)
		{
		    if (IsClientInGame(i) && client != i && HurtPlayerTeam == GetClientTeam(i))
		    {
		    	if(StrEqual(weapon, "hegrenade")) {
		    		DealDamage(i, dmg, attacker, DMG_BLAST, "");
		    	} else if(StrEqual(weapon, "inferno")) {
		    		DealDamage(i, dmg, attacker, DMG_BURN, "");
		    	}
		    }
		} 	
	}
	
	return Plugin_Continue;
} 

// Spawn new flashbangs and detonate them instantly infront of teammates faces
public void SpawnFlashOnLocation(const int thrower)
{
	// To avoid infinity loop (new flashes infront of teammates will trigger the flashed event)
	realFlash = false; 
	
	float PlayerOrg[3]; 
	float PlayerAng[3];
	GetClientEyePosition(thrower, PlayerOrg);
	GetClientEyeAngles(thrower, PlayerAng);
	
	// Get position infront of player
	PlayerOrg[0] += 5 * Cosine(DegToRad(PlayerAng[1])); 
	PlayerOrg[1] += 5 * Sine(DegToRad(PlayerAng[1]));
	PlayerOrg[2] -= 10;
	
	// Create flash entity
	int entity = CreateEntityByName("flashbang_projectile");
	
	if(entity == -1)
		return;
	
	// Set entity properties	
	SetVariantString("OnUser1 !self,InitializeSpaswnFromWorld,,0.0,1");
	AcceptEntityInput(entity, "AddOutput");
	AcceptEntityInput(entity, "FireUser1");
	DispatchSpawn(entity); 
	SetEntProp(entity, Prop_Data, "m_iTeamNum", GetClientTeam(thrower));
	SetEntProp(entity, Prop_Data, "m_takedamage", 2);
	SetEntProp(entity, Prop_Data, "m_iHealth", 2);
	
	// Teleport the flashbang infront to location (infront of a teammates face hehe)
	TeleportEntity(entity, PlayerOrg, NULL_VECTOR, NULL_VECTOR);
	
	// Damage the flashbang entity which will cause it to detonate
	SDKHooks_TakeDamage(entity, entity, thrower, float(100), DMG_GENERIC, -1, NULL_VECTOR, NULL_VECTOR);
	
	// The flashes has now all been created and detonated and by now its safe to turn this on again
	realFlash = true;
}

// Math to get position infront of entity (we detonate flashes infront of their face basically)
stock void AddInFrontOf( float fOrigin[3], float fAngles[3], float fDistanceToFront, float fOutput[3] )
{
    float fDir[3];

    GetAngleVectors(fAngles, fDir, NULL_VECTOR, NULL_VECTOR);
    ScaleVector(fDir, fDistanceToFront);
    AddVectors(fOrigin, fDir, fOutput);               
} 

// Deal damage to entity
public void DealDamage(int victim, int damage, int attacker, int dmg_type, const char[] weapon)
{
	if(victim>0 && IsValidEdict(victim) && IsClientInGame(victim) && IsPlayerAlive(victim) && damage>0)
	{
		char[] dmg_str = "";
		IntToString(damage, dmg_str, 16);
		char[] dmg_type_str = "";
		IntToString(dmg_type,dmg_type_str,32);
		
		int pointHurt=CreateEntityByName("point_hurt");
		if(pointHurt)
		{
			DispatchKeyValue(victim,"targetname","war3_hurtme");
			DispatchKeyValue(pointHurt,"DamageTarget","war3_hurtme");
			DispatchKeyValue(pointHurt,"Damage",dmg_str);
			DispatchKeyValue(pointHurt,"DamageType",dmg_type_str);
			if(!StrEqual(weapon,""))
			{
				DispatchKeyValue(pointHurt,"classname",weapon);
			}
			DispatchSpawn(pointHurt);
			AcceptEntityInput(pointHurt,"Hurt",(attacker>0)?attacker:-1);
			DispatchKeyValue(pointHurt,"classname","point_hurt");
			DispatchKeyValue(victim,"targetname","war3_donthurtme");
			RemoveEdict(pointHurt);
		}
	}
}